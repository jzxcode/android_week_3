package com.jzx.android3;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jzx.android3.sqlite.MySqlite;

public class RegisterActivity extends Activity {
	EditText register_username, register_password, register_again_password;
	MySqlite mysqlite;
	SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		initView();
	}

	private void initView() {
		register_username = (EditText) findViewById(R.id.register_username);
		register_password = (EditText) findViewById(R.id.register_password);
		register_again_password = (EditText) findViewById(R.id.register_again_password);
		
		mysqlite = new MySqlite(this);
		db = mysqlite.getReadableDatabase();
	}

	/**
	 * 注册按钮的点击事件
	 * 
	 * @param view
	 */
	public void register(View view) {
		// 获取输入框内容
		String username = register_username.getText().toString().trim();
		String password = register_password.getText().toString().trim();
		String again_password = register_again_password.getText().toString()
				.trim();

		if (username.equals("")) {
			Toast.makeText(this, "请输入用户名", Toast.LENGTH_SHORT).show();
			return;
		} else if (password.equals("")) {
			Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
			return;
		} else if (again_password.equals("")) {
			Toast.makeText(this, "请再次输入密码验证", Toast.LENGTH_SHORT).show();
			return;
		} else if (!password.equals(again_password)){
			Toast.makeText(this, "两次密码输入不一致", Toast.LENGTH_SHORT).show();
			return;
		}
		//往数据库中插入注册用户信息
		ContentValues values = new ContentValues();
		values.put(MySqlite.USER_TABLE_USERNAME, username);
		values.put(MySqlite.USER_TABLE_PASSWORD, password);
		long result = db.insert(MySqlite.USER_TABLE, null, values);
		
		//注册成功发送通知
		if(result > 0){
			//创建跳转至登录界面的意图
			Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
			//创建pendingIntent对象
			PendingIntent pi = PendingIntent.getActivity(getApplicationContext(), 1, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			//设置通知栏样式
			Notification notification = new NotificationCompat.Builder(RegisterActivity.this)
										.setContentTitle("用户注册成功")
										.setContentText("用户:"+username+",注册成功，快去登录吧!")
										.setSmallIcon(R.drawable.ic_launcher)
										.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher))
										.setWhen(System.currentTimeMillis())
										.setAutoCancel(true)
										.setContentIntent(pi)
										.build();
			
			NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			notificationManager.notify(1, notification);
		}else {
			Toast.makeText(RegisterActivity.this, "用户注册失败!", Toast.LENGTH_SHORT).show();
		}
		
	}

}
