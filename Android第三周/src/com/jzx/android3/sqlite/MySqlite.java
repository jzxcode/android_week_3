package com.jzx.android3.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySqlite extends SQLiteOpenHelper{
	public static final String SQLITE_NAME = "jzx.db";
	public static final int SQLITE_VERSION = 1;
	public static final String USER_TABLE = "user";
	public static final String USER_TABLE_USERNAME = "username";
	public static final String USER_TABLE_PASSWORD = "password";
	
	public MySqlite(Context context) {
		super(context, SQLITE_NAME, null, SQLITE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String createUserTable = "create table user(_id integer primary key autoincrement,username text,password text)";
		db.execSQL(createUserTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}

}
