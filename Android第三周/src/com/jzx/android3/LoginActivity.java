package com.jzx.android3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jzx.android3.sqlite.MySqlite;

public class LoginActivity extends Activity {
	EditText login_username, login_password;
	MySqlite mysqlite;
	SQLiteDatabase db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initView();
	}

	private void initView() {
		login_username = (EditText) findViewById(R.id.login_username);
		login_password = (EditText) findViewById(R.id.login_password);
		mysqlite = new MySqlite(this);
		db = mysqlite.getReadableDatabase();
	}

	/**
	 * 登录按钮点击事件
	 * 
	 * @param view
	 */
	public void login(View view) {
		// 获取输入框内容
		String username = login_username.getText().toString().trim();
		String password = login_password.getText().toString().trim();
		// 验证是否为空
		if (username.equals("")) {
			Toast.makeText(this, "请输入用户名", Toast.LENGTH_SHORT).show();
			return;
		} else if (password.equals("")) {
			Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
			return;
		}
		// 判断用户名和密码是否正确
		Cursor cursor = db.query(MySqlite.USER_TABLE, null,
				MySqlite.USER_TABLE_USERNAME + "=?", new String[] { username },
				null, null, null);
		if(cursor.moveToNext()){
			String cursorPassword = cursor.getString(cursor.getColumnIndex(MySqlite.USER_TABLE_PASSWORD));
			if(password.equals(cursorPassword)){
				Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
				startActivity(intent);
				this.finish();
			}else{
				Toast.makeText(this, "用户密码错误", Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(this, "用户不存在", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 注册按钮的点击事件
	 * 
	 * @param view
	 */
	public void jumpRegister(View view) {
		Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
		startActivity(intent);
	}

}
